FROM amazoncorretto:11
MAINTAINER root@ahoora.se
EXPOSE 8080
VOLUME /log
ARG JAR_FILE_ARG
ADD target/thin/root/repository m2/repository
ENV JAVA_OPTS=""
ADD target/thin/root/${JAR_FILE_ARG} app.jar
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar --thin.root=/m2" ]
