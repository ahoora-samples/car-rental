package se.ahoora.carrental.exception;


import java.util.Objects;

public class ParseError<T, E extends Exception> {
    private T source;
    private E cause;


    public ParseError(T source, E cause) {
        this.source = source;
        this.cause = cause;
    }


    public T getSource() {
        return source;
    }

    public void setSource(T source) {
        this.source = source;
    }

    public E getCause() {
        return cause;
    }

    public void setCause(E cause) {
        this.cause = cause;
    }



    @Override
    public String toString() {
        return "{" + "source: " + source + ", cause: " + cause + "}";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParseError)) return false;
        final ParseError<?, ?> that = (ParseError<?, ?>) o;
        return Objects.equals(getSource(), that.getSource()) && Objects.equals(getCause(), that.getCause());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSource(), getCause());
    }
}
