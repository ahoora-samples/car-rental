package se.ahoora.carrental.exception;

public class DataException extends RuntimeException {

    public DataException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public DataException(Throwable err) {
        super(err);
    }

    public DataException(String message) {
        super(message);
    }
}
