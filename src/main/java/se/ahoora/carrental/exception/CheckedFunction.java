package se.ahoora.carrental.exception;

@FunctionalInterface
public interface CheckedFunction<T, E> {
    E apply(T t) throws Exception;
}
