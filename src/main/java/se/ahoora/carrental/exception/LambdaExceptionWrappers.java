package se.ahoora.carrental.exception;

import lombok.extern.log4j.Log4j2;

import java.util.function.Function;

@Log4j2
public class LambdaExceptionWrappers {

    public static <T, R> Function<T, Either> lift(CheckedFunction<T, R> function) {
        return i -> {
            try {
                return Either.Right(function.apply(i));
            } catch (Exception e) {
                log.warn(e);
                return Either.Left(new ParseError(i, e));
            }
        };
    }
}