package se.ahoora.carrental.facade.api;

import se.ahoora.carrental.dto.BookedCarReportDTO;
import se.ahoora.carrental.dto.TotalDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface ReportFacade {

    List<BookedCarReportDTO> bookedCarsByFromDateTimeAndToDateTimeReport(LocalDateTime fromDateTime, LocalDateTime toDateTime);

    Map<String, Object> numberOfBookedCarsPerHourByFromDateToToDate(LocalDate fromDate, LocalDate toDate);

    TotalDTO totalPaymentByFromDateToToDate(LocalDate fromDate, LocalDate toDate);

}
