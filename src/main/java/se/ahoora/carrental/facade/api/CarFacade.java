package se.ahoora.carrental.facade.api;

import se.ahoora.carrental.dto.AvailabilityCreateDTO;
import se.ahoora.carrental.dto.CarCreateDTO;
import se.ahoora.carrental.dto.CarDTO;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface CarFacade {

     String create(CarCreateDTO car);

     String addAvailability(String carId, AvailabilityCreateDTO availability);

     List<CarDTO> search(LocalDateTime from, LocalDateTime to, BigDecimal maximumHourlyRate);
}
