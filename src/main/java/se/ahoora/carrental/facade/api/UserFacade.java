package se.ahoora.carrental.facade.api;

import se.ahoora.carrental.dto.UserCreateDTO;
import se.ahoora.carrental.dto.UserDTO;
import se.ahoora.carrental.dto.UserUpdateDTO;

import java.util.List;

public interface UserFacade {

    String create(UserCreateDTO user);

    void update(UserUpdateDTO user);

    UserDTO findById(String id);

    List<UserDTO> findAll();

    void delete(String id);

}
