package se.ahoora.carrental.facade.api;

import se.ahoora.carrental.dto.BookCreateDTO;

public interface BookFacade {

    String create(BookCreateDTO car);
}
