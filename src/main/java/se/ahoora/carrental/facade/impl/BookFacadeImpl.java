package se.ahoora.carrental.facade.impl;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.ahoora.carrental.dto.BookCreateDTO;
import se.ahoora.carrental.facade.api.BookFacade;
import se.ahoora.carrental.service.api.BookService;

@Slf4j
@Component("bookFacade")
public class BookFacadeImpl implements BookFacade {

    private BookService bookService;
    private ModelMapper modelMapper;


    @Autowired
    public BookFacadeImpl(BookService bookService, ModelMapper modelMapper) {
        this.bookService = bookService;
        this.modelMapper = modelMapper;
    }

    @Override
    public String create(BookCreateDTO book) {
        return this.bookService.create(book);
    }

}
