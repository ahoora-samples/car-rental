package se.ahoora.carrental.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.ahoora.carrental.dto.BookedCarReportDTO;
import se.ahoora.carrental.dto.TotalDTO;
import se.ahoora.carrental.facade.api.ReportFacade;
import se.ahoora.carrental.service.api.ReportService;

import java.time.*;
import java.util.List;
import java.util.Map;

@Component("reportFacade")
public class ReportFacadeImpl implements ReportFacade {

    private final ReportService reportService;

    @Autowired
    public ReportFacadeImpl(ReportService reportService) {
        this.reportService = reportService;
    }

    @Override
    public List<BookedCarReportDTO> bookedCarsByFromDateTimeAndToDateTimeReport(LocalDateTime fromDateTime, LocalDateTime toDateTime) {
        return this.reportService.bookedCarsByFromDateTimeAndToDateTimeReport(fromDateTime.toInstant(ZoneOffset.UTC), toDateTime.toInstant(ZoneOffset.UTC));
    }

    @Override
    public Map<String, Object> numberOfBookedCarsPerHourByFromDateToToDate(LocalDate fromDate, LocalDate toDate) {
        return this.reportService.numberOfBookedCarsPerHourByFromDateToToDate(this.convert(fromDate), this.convert(toDate));
    }

    @Override
    public TotalDTO totalPaymentByFromDateToToDate(LocalDate fromDate, LocalDate toDate) {
        return this.reportService.totalPaymentByFromDateToToDate(this.convert(fromDate), this.convert(toDate));
    }


    private Instant convert(LocalDate input) {
        return input.atStartOfDay(ZoneId.systemDefault()).toInstant();
    }

}
