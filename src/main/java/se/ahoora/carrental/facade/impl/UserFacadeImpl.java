package se.ahoora.carrental.facade.impl;

import lombok.val;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.ahoora.carrental.domain.User;
import se.ahoora.carrental.dto.UserCreateDTO;
import se.ahoora.carrental.dto.UserDTO;
import se.ahoora.carrental.dto.UserUpdateDTO;
import se.ahoora.carrental.service.api.UserService;

import java.util.List;

@Component("userFacade")
public class UserFacadeImpl implements se.ahoora.carrental.facade.api.UserFacade {

    private UserService userService;
    private ModelMapper modelMapper;

    @Autowired
    public UserFacadeImpl(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @Override
    public String create(UserCreateDTO user) {
        return this.userService.create(this.modelMapper.map(user, User.class));
    }

    @Override
    public void update(UserUpdateDTO user) {
        this.userService.update(this.modelMapper.map(user, User.class));
    }

    @Override
    public UserDTO findById(String id) {
        return this.modelMapper.map(this.userService.findById(id), UserDTO.class);
    }

    @Override
    public List<UserDTO> findAll() {
        val src = this.userService.findAll();

        return modelMapper.map(src, new TypeToken<List<UserDTO>>() {}.getType());
    }

    @Override
    public void delete(String id) {
        this.userService.delete(id);
    }


}
