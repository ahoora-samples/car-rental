package se.ahoora.carrental.facade.impl;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.ahoora.carrental.domain.Availability;
import se.ahoora.carrental.domain.Car;
import se.ahoora.carrental.dto.AvailabilityCreateDTO;
import se.ahoora.carrental.dto.CarCreateDTO;
import se.ahoora.carrental.dto.CarDTO;
import se.ahoora.carrental.facade.api.CarFacade;
import se.ahoora.carrental.service.api.CarService;
import se.ahoora.carrental.service.api.UserService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Slf4j
@Component("carFacade")
public class CarFacadeImpl implements CarFacade {

    private CarService carService;
    private UserService userService;
    private ModelMapper modelMapper;

    @Autowired
    public CarFacadeImpl(CarService carService, UserService userService, ModelMapper modelMapper) {
        this.carService = carService;
        this.userService = userService;
        this.modelMapper = modelMapper;

    }


    @Override
    public String create(CarCreateDTO car) {
        final Car result = modelMapper.map(car, new TypeToken<Car>() {
        }.getType());
        result.setOwner(this.userService.findById(car.getOwnerId()));
        return this.carService.create(result);
    }

    @Override
    public String addAvailability(String carId, AvailabilityCreateDTO availability) {
        val avail = Availability.builder()
                .carId(carId)
                .from(availability.getFrom().toInstant(ZoneOffset.UTC))
                .to(availability.getTo().toInstant(ZoneOffset.UTC))
                .hourlyRate(availability.getHourlyRate())
                .build();
        return this.carService.addAvailability(carId, avail);
    }

    @Override
    public List<CarDTO> search(LocalDateTime from, LocalDateTime to, BigDecimal maximumHourlyRate) {
        val result = this.carService.search(
                from != null ? from.toInstant(ZoneOffset.UTC) : null,
                to != null ? to.toInstant(ZoneOffset.UTC) : null,
                maximumHourlyRate);
        return modelMapper.map(result, new TypeToken<List<CarDTO>>() {
        }.getType());
    }
}
