package se.ahoora.carrental;

import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Log4j2
@ComponentScan(basePackages = "se.ahoora.carrental")
@EnableSwagger2
@SpringBootApplication
public class Application {

    @Bean
    public Map<Integer, String> colorMap() {
        return new ConcurrentHashMap<>();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    @Scope("singleton")
    public ModelMapper modelMapper() {
        val modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
        return modelMapper;
    }

}