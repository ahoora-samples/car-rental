package se.ahoora.carrental.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    @NotBlank(message = "id is mandatory")
    private String id;

    @NotNull(message = "car is mandatory")
    private Car car;

    @NotNull(message = "customer is mandatory")
    private User customer;

    private Instant time;

}
