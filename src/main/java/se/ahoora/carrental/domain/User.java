package se.ahoora.carrental.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @NotBlank(message = "id is mandatory")
    private String id;

    @NotBlank(message = "username is mandatory")
    private String username;

    @NotNull(message = "user type is mandatory")
    private UserType type;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String address;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
