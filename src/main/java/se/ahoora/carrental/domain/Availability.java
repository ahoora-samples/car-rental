package se.ahoora.carrental.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Availability implements Cloneable {

    @NotBlank(message = "id is mandatory")
    private String id;

    @NotBlank(message = "carId is mandatory")
    private String carId;

    @NotNull(message = "from date is mandatory")
    private Instant from;

    @NotNull(message = "to date is mandatory")
    private Instant to;

    @NotNull(message = "hourlyRate is mandatory")
    private BigDecimal hourlyRate;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
