package se.ahoora.carrental.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Car implements Cloneable {

    @NotBlank(message = "id is mandatory")
    private String id;

    @NotNull(message = "owner is mandatory")
    private User owner;

    @NotBlank(message = "plateNumber is mandatory")
    @Pattern(regexp = "^[A-Z]{3}[0-9]{2}[A-Za-z0-9]{1}$", message = "bad plateNumber")
    private String plateNumber;

    @Builder.Default
    private Map<String, Availability> availabilities = new ConcurrentHashMap<>();

    @Builder.Default
    private Map<String, String> attributes = new ConcurrentHashMap<>();

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
