package se.ahoora.carrental.domain;

public enum UserType {
    OWNER, CUSTOMER, OPERATOR,ANONYMOUS
}
