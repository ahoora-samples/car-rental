package se.ahoora.carrental.service.impl;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.ahoora.carrental.domain.Book;
import se.ahoora.carrental.domain.Car;
import se.ahoora.carrental.dto.BookCreateDTO;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.repository.api.BookRepo;
import se.ahoora.carrental.repository.api.CarRepo;
import se.ahoora.carrental.repository.api.UserRepo;
import se.ahoora.carrental.service.api.BookService;

import java.time.Instant;
import java.util.List;

@Slf4j
@Service("bookService")
public class BookServiceImpl implements BookService {
    private BookRepo bookRepo;
    private CarRepo carRepo;
    private UserRepo userRepo;


    @Autowired
    public BookServiceImpl(BookRepo bookRepo, UserRepo userRepo, CarRepo carRepo) {
        this.bookRepo = bookRepo;
        this.carRepo = carRepo;
        this.userRepo = userRepo;
    }

    @Override
    public String create(Book book) {
        return this.bookRepo.create(book);
    }

    @Override
    public void update(Book book) {
        this.bookRepo.update(book);
    }

    @Override
    public Book delete(String id) {
        return this.bookRepo.delete(id);
    }

    @Override
    public List<Book> findAll() {
        return this.bookRepo.findAll();
    }

    @Override
    public Book findById(String id) {
        return this.bookRepo.findById(id);
    }

    @Override
    public String create(BookCreateDTO bookDTO) {
        return this.bookRepo.create(this.convert(bookDTO));
    }

    Book convert(BookCreateDTO bookDTO) {
        val dbCar = this.carRepo.findById(bookDTO.getCarId());
        log.info("book db car: {}", dbCar.toString());
        val customer = this.userRepo.findById(bookDTO.getCustomerId());
        log.info("booking customer: {}", customer.toString());
        val car = this.copy(dbCar);
        dbCar.getAvailabilities().remove(bookDTO.getAvailabilityId());
        car.getAvailabilities().keySet().removeIf(k -> !k.equals(bookDTO.getAvailabilityId()));
        return Book.builder().car(car).customer(customer).time(Instant.now()).build();
    }


    Car copy(Car car) {
        try {
            return (Car) car.clone();
        } catch (Exception e) {
            throw new DataException(e.getMessage());
        }
    }

}
