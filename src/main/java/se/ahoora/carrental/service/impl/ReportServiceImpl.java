package se.ahoora.carrental.service.impl;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.ahoora.carrental.domain.Availability;
import se.ahoora.carrental.domain.Book;
import se.ahoora.carrental.dto.BookedCarReportDTO;
import se.ahoora.carrental.dto.TotalDTO;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.repository.api.BookRepo;
import se.ahoora.carrental.service.api.ReportService;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("reportService")
public class ReportServiceImpl implements ReportService {

    private BookRepo bookRepo;

    @Autowired
    public ReportServiceImpl(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }

    @Override
    public List<BookedCarReportDTO> bookedCarsByFromDateTimeAndToDateTimeReport(Instant from, Instant to) {
        return this.bookRepo.findAll().stream()
                .filter(i ->
                        i.getTime().truncatedTo(ChronoUnit.HOURS).isAfter(from.truncatedTo(ChronoUnit.HOURS)) ||
                                i.getTime().truncatedTo(ChronoUnit.HOURS).equals(from.truncatedTo(ChronoUnit.HOURS)))

                .filter(i ->
                        i.getTime().truncatedTo(ChronoUnit.HOURS).isBefore(to.truncatedTo(ChronoUnit.HOURS)) ||
                                i.getTime().truncatedTo(ChronoUnit.HOURS).equals(to.truncatedTo(ChronoUnit.HOURS)))

                .map(i -> BookedCarReportDTO.builder()
                        .bookId(i.getId())
                        .carId(i.getCar().getId())
                        .plateNumber(i.getCar().getPlateNumber())
                        .owner(i.getCar().getOwner().getUsername())
                        .customer(i.getCustomer().getUsername())
                        .time(i.getTime())
                        .build())

                .collect(Collectors.toList());
    }

    @Override
    public Map<String, Object> numberOfBookedCarsPerHourByFromDateToToDate(Instant from, Instant to) {

        this.bookRepo.findAll().stream()
                .filter(i ->
                        i.getTime().truncatedTo(ChronoUnit.HOURS).isAfter(from.truncatedTo(ChronoUnit.HOURS)) ||
                                i.getTime().truncatedTo(ChronoUnit.HOURS).equals(from.truncatedTo(ChronoUnit.HOURS)))

                .filter(i ->
                        i.getTime().truncatedTo(ChronoUnit.HOURS).isBefore(to.truncatedTo(ChronoUnit.HOURS)) ||
                                i.getTime().truncatedTo(ChronoUnit.HOURS).equals(to.truncatedTo(ChronoUnit.HOURS)))


                .collect(Collectors.toList());

        throw new DataException("not implemented");
    }

    @Override
    public TotalDTO totalPaymentByFromDateToToDate(Instant from, Instant to) {
        val total = this.bookRepo.findAll().stream()
                .filter(i ->
                        i.getTime().truncatedTo(ChronoUnit.HOURS).isAfter(from.truncatedTo(ChronoUnit.HOURS)) ||
                                i.getTime().truncatedTo(ChronoUnit.HOURS).equals(from.truncatedTo(ChronoUnit.HOURS)))

                .filter(i ->
                        i.getTime().truncatedTo(ChronoUnit.HOURS).isBefore(to.truncatedTo(ChronoUnit.HOURS)) ||
                                i.getTime().truncatedTo(ChronoUnit.HOURS).equals(to.truncatedTo(ChronoUnit.HOURS)))

                .map(this::calculatePayment)

                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return TotalDTO.builder().total(total).build();
    }


    BigDecimal calculatePayment(Book book) {
        val availability = (Availability) book.getCar().getAvailabilities().values().toArray()[0];
        val hours = Duration.between(availability.getFrom(), availability.getTo()).toHours();
        return new BigDecimal(hours).multiply(availability.getHourlyRate());
    }


}
