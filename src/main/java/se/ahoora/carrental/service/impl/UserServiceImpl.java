package se.ahoora.carrental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.ahoora.carrental.domain.User;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.repository.api.CarRepo;
import se.ahoora.carrental.repository.api.UserRepo;
import se.ahoora.carrental.service.api.UserService;

import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    private UserRepo userRepo;
    private CarRepo carRepo;

    @Autowired
    public UserServiceImpl(UserRepo userRepo, CarRepo carRepo) {
        this.userRepo = userRepo;
        this.carRepo = carRepo;
    }

    @Override
    public String create(User user) {
        return this.userRepo.create(user);
    }

    @Override
    public void update(User user) {
        this.userRepo.update(user);
    }

    @Override
    public User delete(String id) {
        if (carRepo.findAll().stream().anyMatch(i -> i.getOwner().getId().equals(id)))
            throw new DataException(String.format("can't delete user [%s]", id));
        return this.userRepo.delete(id);
    }

    @Override
    public List<User> findAll() {
        return this.userRepo.findAll();
    }

    @Override
    public User findById(String id) {
        return this.userRepo.findById(id);
    }
}
