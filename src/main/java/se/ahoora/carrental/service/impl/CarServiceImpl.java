package se.ahoora.carrental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.ahoora.carrental.domain.Availability;
import se.ahoora.carrental.domain.Car;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.repository.api.BookRepo;
import se.ahoora.carrental.repository.api.CarRepo;
import se.ahoora.carrental.service.api.CarService;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Service("carService")
public class CarServiceImpl implements CarService {

    private CarRepo carRepo;
    private BookRepo bookRepo;

    @Autowired
    public CarServiceImpl(CarRepo carRepo, BookRepo bookRepo) {
        this.carRepo = carRepo;
        this.bookRepo = bookRepo;
    }


    @Override
    public String create(Car car) {
        return this.carRepo.create(car);
    }

    @Override
    public void update(Car car) {
        this.carRepo.update(car);
    }

    @Override
    public Car delete(String id) {
        if (this.bookRepo.findAll().stream().anyMatch(i -> i.getCar().getId().equals(id)))
            throw new DataException(String.format("can't delete car [%s]", id));
        return this.carRepo.delete(id);
    }

    @Override
    public List<Car> findAll() {
        return this.carRepo.findAll();
    }

    @Override
    public Car findById(String id) {
        return this.carRepo.findById(id);
    }

    @Override
    public List<Car> search(Instant from, Instant to, BigDecimal maximumHourlyRate) {
        if (from == null && to == null && maximumHourlyRate == null)
            return this.carRepo.findAll();
        else
            return this.carRepo.search(from, to, maximumHourlyRate);
    }

    @Override
    public String addAvailability(String carId, Availability availability) {
        return this.carRepo.addAvailability(carId, availability);
    }

    @Override
    public void removeAvailability(String carId, String availabilityId) {
        this.carRepo.removeAvailability(carId, availabilityId);
    }

    @Override
    public void editAvailability(String carId, Availability availability) {
        this.carRepo.editAvailability(carId, availability);
    }
}
