package se.ahoora.carrental.service.api;

import se.ahoora.carrental.dto.BookedCarReportDTO;
import se.ahoora.carrental.dto.TotalDTO;

import java.time.Instant;
import java.util.List;
import java.util.Map;

public interface ReportService {

    List<BookedCarReportDTO> bookedCarsByFromDateTimeAndToDateTimeReport(Instant from, Instant to);

    Map<String, Object> numberOfBookedCarsPerHourByFromDateToToDate(Instant from, Instant to);

    TotalDTO totalPaymentByFromDateToToDate(Instant from, Instant to);

}
