package se.ahoora.carrental.service.api;

import se.ahoora.carrental.domain.User;

public interface UserService extends Service<String, User> {

}
