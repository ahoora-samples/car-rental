package se.ahoora.carrental.service.api;

import se.ahoora.carrental.domain.Book;
import se.ahoora.carrental.dto.BookCreateDTO;

public interface BookService extends Service<String, Book> {
    String create(BookCreateDTO book);
}
