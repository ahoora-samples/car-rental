package se.ahoora.carrental.service.api;

import java.util.List;

public interface Service<T, S> {

    T create(S entity);

    void update(S entity);

    S delete(T id);

    List<S> findAll();

    S findById(T id);
}
