package se.ahoora.carrental.service.api;

import se.ahoora.carrental.domain.Availability;
import se.ahoora.carrental.domain.Car;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

public interface CarService extends Service<String, Car> {

    List<Car> search(Instant from, Instant to, BigDecimal maximumHourlyRate);

    String addAvailability(String carId, Availability availability);

    void removeAvailability(String carId, String availabilityId);

    void editAvailability(String carId, Availability availability);
}
