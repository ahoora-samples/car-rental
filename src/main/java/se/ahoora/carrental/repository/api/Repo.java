package se.ahoora.carrental.repository.api;

import java.util.List;
import java.util.Map;

public interface Repo<T, S> {

    void setTable(Map<T,S> table);

    T create(S entity);

    void update(S entity);

    S delete(T id);

    List<S> findAll();

    S findById(T id);

    default boolean exist(Map<String, ?> table, String id) {
        return table.containsKey(id);
    }

}
