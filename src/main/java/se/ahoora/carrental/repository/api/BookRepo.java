package se.ahoora.carrental.repository.api;

import se.ahoora.carrental.domain.Book;

public interface BookRepo extends Repo<String, Book> {
}
