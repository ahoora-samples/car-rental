package se.ahoora.carrental.repository.api;

import se.ahoora.carrental.domain.User;

public interface UserRepo extends Repo<String, User> {
}
