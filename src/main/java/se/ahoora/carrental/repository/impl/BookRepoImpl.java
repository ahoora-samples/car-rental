package se.ahoora.carrental.repository.impl;

import lombok.val;
import org.springframework.stereotype.Repository;
import se.ahoora.carrental.domain.Book;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.repository.api.BookRepo;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository("bookRepo")
public class BookRepoImpl implements BookRepo {

    private Map<String, Book> table = new ConcurrentHashMap<>();

    public BookRepoImpl() {
        super();
    }


    @Override
    public void setTable(Map<String, Book> table) {
        this.table = table;
    }

    @Override
    public String create(@NotNull(message = "book is mandatory") Book entity) {
        val uuid = UUID.randomUUID();
        val id = uuid.toString();
        entity.setId(id);
        this.table.put(id, entity);
        return id;
    }

    @Override
    public void update(@NotNull(message = "book is mandatory") Book entity) {
        val id = entity.getId();
        if (this.table.containsKey(id)) {
            this.table.put(id, entity);
        } else
            throw new DataException(String.format("book not found [%s]", id));
    }

    @Override
    public Book delete(String id) {
        return (Book) this.table.remove(id);
    }

    @Override
    public List<Book> findAll() {
        return new ArrayList<Book>(this.table.values());
    }

    @Override
    public Book findById(String id) {
        if (this.table.containsKey(id))
            return this.table.get(id);
        else
            throw new DataException(String.format("book not found [%s]", id));
    }
}
