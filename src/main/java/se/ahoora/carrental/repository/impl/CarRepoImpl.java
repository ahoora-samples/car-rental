package se.ahoora.carrental.repository.impl;

import io.micrometer.core.lang.Nullable;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Repository;
import se.ahoora.carrental.domain.Availability;
import se.ahoora.carrental.domain.Car;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.repository.api.CarRepo;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Slf4j
@Repository("carRepo")
public class CarRepoImpl implements CarRepo {

    private Map<String, Car> table = new ConcurrentHashMap<>();

    public CarRepoImpl() {
        super();
    }

    @Override
    public void setTable(Map<String, Car> table) {
        this.table = table;
    }

    @Override
    public String create(Car entity) {
        if (this.table.values().stream().anyMatch(i -> i.getPlateNumber().equals(entity.getPlateNumber())))
            throw new DataException(String.format("the plate number has been registered [%s]", entity.getPlateNumber()));
        val uuid = UUID.randomUUID();
        val id = uuid.toString();
        entity.setId(id);
        this.table.put(id, entity);
        return id;
    }

    @Override
    public void update(Car entity) {
        val id = entity.getId();
        if (exist(table, id)) {
            this.table.put(id, entity);
        } else
            throw new DataException(String.format("car not found [%s]", id));
    }

    @Override
    public List<Car> findAll() {
        return new ArrayList<Car>(this.table.values());
    }

    @Override
    public Car findById(String id) {
        if (exist(table, id))
            return this.table.get(id);
        else
            throw new DataException(String.format("car not found [%s]", id));
    }


    @Override
    public Car delete(String id) {
        return this.table.remove(id);
    }


    @Override
    public List<Car> search(@Nullable Instant from, @Nullable Instant to, @Nullable BigDecimal maxRate) {
        val availabilities = flattenAvailabilities(this.table);
        val a = this.searchMaxRateCriteria(availabilities, maxRate);
        val b = this.searchDateCriteria(a, from, to);
        return this.getCarWithSingleAvailability(b);
    }

    List<Availability> flattenAvailabilities(Map<String, Car> table) {
        return table.values()
                .stream()
                .filter(i -> !i.getAvailabilities().isEmpty())
                .flatMap(i -> i.getAvailabilities().values().stream()).collect(Collectors.toList());
    }


    List<Availability> searchDateCriteria(List<Availability> input, Instant from, Instant to) {
        return input.stream()
                .filter(i -> from == null || i.getFrom().truncatedTo(ChronoUnit.HOURS).isAfter(from.truncatedTo(ChronoUnit.HOURS)) ||
                        i.getFrom().truncatedTo(ChronoUnit.HOURS).equals(from.truncatedTo(ChronoUnit.HOURS)))

                .filter(i -> to == null || (i.getTo().truncatedTo(ChronoUnit.HOURS).isAfter(to.truncatedTo(ChronoUnit.HOURS)) ||
                        i.getTo().truncatedTo(ChronoUnit.HOURS).equals(to.truncatedTo(ChronoUnit.HOURS))))

                .collect(Collectors.toList());
    }

    List<Availability> searchMaxRateCriteria(List<Availability> input, BigDecimal maxRate) {
        return input.stream().filter(i -> maxRate == null || maxRate.compareTo(i.getHourlyRate()) > -1)
                .collect(Collectors.toList());
    }


    public List<Car> getCarWithSingleAvailability(List<Availability> input) {
        return input.stream().map(i -> {
                    val availMap = new ConcurrentHashMap<String, Availability>();
                    availMap.put(i.getId(), i);
                    val originalCar = table.get(i.getCarId());
                    return Car.builder()
                            .id(i.getCarId())
                            .owner(originalCar.getOwner())
                            .plateNumber(originalCar.getPlateNumber())
                            .attributes(originalCar.getAttributes())
                            .availabilities(availMap)
                            .build();
                }
        ).collect(Collectors.toList());
    }

    @Override
    public String addAvailability(String carId, Availability availability) {
        if (exist(this.table, carId)) {
            val id = UUID.randomUUID().toString();
            availability.setId(id);
            availability.setCarId(carId);
            val car = this.table.get(carId);
            car.getAvailabilities().put(id, availability);
            return id;
        } else
            throw new DataException(String.format("car not found [%s]", carId));
    }

    @Override
    public void removeAvailability(String carId, String availabilityId) {
        if (exist(this.table, carId))
            this.table.get(carId).getAvailabilities().remove(availabilityId);
        else
            throw new DataException(String.format("car not found [%s]", carId));
    }

    @Override
    public void editAvailability(String carId, Availability availability) {
        if (exist(this.table, carId)) {
            this.table.get(carId).getAvailabilities().put(availability.getId(), availability);
        } else
            throw new DataException(String.format("car not found [%s]", carId));
    }
}
