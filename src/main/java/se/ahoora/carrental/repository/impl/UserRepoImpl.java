package se.ahoora.carrental.repository.impl;

import lombok.val;
import org.springframework.stereotype.Repository;
import se.ahoora.carrental.domain.User;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.repository.api.UserRepo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository("userRepo")
public class UserRepoImpl implements UserRepo {

    private Map<String, User> table = new ConcurrentHashMap<String, User>();

    public UserRepoImpl() {
        super();
    }

    @Override
    public void setTable(Map<String, User> table) {
        this.table = table;
    }

    @Override
    public String create(@NotNull(message = "user is mandatory") User entity) {
        val uuid = UUID.randomUUID();
        val id = uuid.toString();
        if (this.table.values().stream().anyMatch(i -> i.getUsername().equals(entity.getUsername())))
            throw new DataException(String.format("username already exists [%s]", entity.getUsername()));
        entity.setId(id);
        this.table.put(id, entity);
        return id;
    }

    @Override
    public void update(@NotNull(message = "user is mandatory") User entity) {
        val id = entity.getId();
        if (this.table.containsKey(id)) {
            entity.setUsername(table.get(id).getUsername());
            this.table.put(id, entity);
        } else
            throw new DataException(String.format("user not found [%s]", id));
    }

    @Override
    public User delete(String id) {
        return (User) this.table.remove(id);
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<User>(this.table.values());
    }

    @Override
    public User findById(@NotBlank(message = "user id is mandatory") String id) {
        if (this.table.containsKey(id))
            return this.table.get(id);
        else
            throw new DataException(String.format("user not found [%s]", id));
    }


}
