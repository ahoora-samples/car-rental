package se.ahoora.carrental;

import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import se.ahoora.carrental.dto.ErrorDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

public class Const {

    private Const() {
        super();
    }

    public static ResponseEntity<ErrorDTO> badRequest(Errors errors) {
        val msg = errors.getAllErrors().stream().map(i -> i.getDefaultMessage()).collect(Collectors.toList()).toString();
        val description = String.format("found %d errors. %s", errors.getErrorCount(), msg);
        return new ResponseEntity<>(ErrorDTO.badRequest(description), HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<ErrorDTO> badRequest(String description) {
        return new ResponseEntity<>(ErrorDTO.badRequest(description), HttpStatus.BAD_REQUEST);
    }

    public static boolean accept(String value, HttpServletRequest request) {
        if (request == null)
            return false;

        if (request.getHeader("Accept") == null)
            return false;

        return request.getHeader("Accept").contains(value);
    }

    public static ResponseEntity<ErrorDTO> internalServerError(String description) {
        return new ResponseEntity<>(ErrorDTO.internalServerError(description), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
