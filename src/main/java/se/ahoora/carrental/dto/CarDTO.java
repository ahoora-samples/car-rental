package se.ahoora.carrental.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.util.Map;

@Data
@Validated
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CarDTO {

    @JsonProperty("id")
    private String id;

    @JsonProperty("plateNumber")
    private String plateNumber;

    @JsonProperty("owner")
    private UserDTO owner;

    @JsonProperty("availabilities")
    private Map<String, AvailabilityDTO> availabilities;

    @JsonProperty("attributes")
    private Map<String, String> attributes;
}
