package se.ahoora.carrental.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;

@Data
@Validated
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TotalDTO {

    @NumberFormat(pattern = "#0.00")
    @JsonProperty("total")
    private BigDecimal total;
}
