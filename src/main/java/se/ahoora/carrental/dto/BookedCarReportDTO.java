package se.ahoora.carrental.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;

import java.time.Instant;

@Data
@Validated
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BookedCarReportDTO {

    @JsonProperty("bookId")
    private String bookId;

    @JsonProperty("carId")
    private String carId;

    @JsonProperty("plateNumber")
    private String plateNumber;

    @JsonProperty("owner")
    private String owner;

    @JsonProperty("customer")
    private String customer;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonProperty("time")
    private Instant time;

}
