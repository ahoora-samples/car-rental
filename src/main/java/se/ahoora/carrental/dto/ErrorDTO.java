package se.ahoora.carrental.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;

@Validated
@JsonInclude(Include.NON_EMPTY)
public class ErrorDTO {

	@JsonProperty("timestamp")
	private Instant timestamp;

	@JsonProperty("status")
	private Integer code = null;

	@JsonProperty("description")
	private String description = null;

	@JsonProperty("error")
	private String reasonPhrase = null;




	public ErrorDTO code(Integer code) {
		this.code = code;
		return this;
	}

	public ErrorDTO timestamp(Instant timestamp) {
		this.timestamp = timestamp;
		return this;
	}


	/**
	 * Get code minimum: 400 maximum: 599
	 * 
	 * @return code
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Min(400)
	@Max(599)
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public ErrorDTO description(String description) {
		this.description = description;
		return this;
	}

	public static ErrorDTO internalServerError(String description) {
		return new ErrorDTO().code(500).timestamp(Instant.now()).reasonPhrase("Internal Server Error").description(description);
	}

	public static ErrorDTO badRequest(String description) {
		return new ErrorDTO().code(400).timestamp(Instant.now()).reasonPhrase("Bad Request").description(description);
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Get description
	 * 
	 * @return description
	 **/
	@ApiModelProperty(example = "Bad query parameter [$size]: Invalid integer value [abc]", value = "")

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ErrorDTO reasonPhrase(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
		return this;
	}

	/**
	 * Get reasonPhrase
	 * 
	 * @return reasonPhrase
	 **/
	@ApiModelProperty(example = "Bad Request", value = "")

	public String getReasonPhrase() {
		return reasonPhrase;
	}

	public void setReasonPhrase(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ErrorDTO errorDTO = (ErrorDTO) o;
		return Objects.equals(this.code, errorDTO.code) && Objects.equals(this.description, errorDTO.description)
				&& Objects.equals(this.reasonPhrase, errorDTO.reasonPhrase);
	}

	@Override
	public int hashCode() {
		return Objects.hash(code, description, reasonPhrase);
	}



	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ErrorDTO {\n");

		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    description: ").append(toIndentedString(description)).append("\n");
		sb.append("    reasonPhrase: ").append(toIndentedString(reasonPhrase)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
