package se.ahoora.carrental.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;

@Data
@Validated
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AvailabilityDTO {


    @JsonProperty("id")
    private String id;

    @JsonProperty("carId")
    private String carId;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonProperty("from")
    private String from;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonProperty("to")
    private String to;

    @NumberFormat(pattern = "#0.00")
    @JsonProperty("hourlyRate")
    private BigDecimal hourlyRate;

}
