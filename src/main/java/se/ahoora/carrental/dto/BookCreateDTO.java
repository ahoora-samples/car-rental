package se.ahoora.carrental.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BookCreateDTO {
    private String carId;
    private String availabilityId;
    private String customerId;

}
