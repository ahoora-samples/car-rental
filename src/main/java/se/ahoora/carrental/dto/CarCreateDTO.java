package se.ahoora.carrental.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Map;

@Data
@Validated
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CarCreateDTO {

    @NotBlank(message = "plateNumber is mandatory")
    @Pattern(regexp = "^[A-Z]{3}[0-9]{2}[A-Za-z0-9]{1}$", message = "bad plateNumber")
    @JsonProperty("plateNumber")
    private String plateNumber;

    @NotBlank(message = "ownerId is mandatory")
    @JsonProperty("ownerId")
    private String ownerId;

    @JsonProperty("attributes")
    private Map<String, String> attributes;
}
