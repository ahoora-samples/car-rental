package se.ahoora.carrental.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import se.ahoora.carrental.domain.UserType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Validated
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserCreateDTO {



    @NotBlank(message = "username is mandatory")
    @JsonProperty("username")
    private String username;

    @NotNull(message = "user type is mandatory")
    @JsonProperty("type")
    private UserType type;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("phoneNumber")
    private String phoneNumber;

    @JsonProperty("address")
    private String address;


}
