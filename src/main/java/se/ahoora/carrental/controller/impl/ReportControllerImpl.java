package se.ahoora.carrental.controller.impl;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import se.ahoora.carrental.controller.api.ReportController;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.facade.api.ReportFacade;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static se.ahoora.carrental.Const.badRequest;
import static se.ahoora.carrental.Const.internalServerError;

@Slf4j
@CrossOrigin(origins = "*", maxAge = -1)
@Controller
public class ReportControllerImpl implements ReportController {

    private ReportFacade reportFacade;

    @Autowired
    public ReportControllerImpl(ReportFacade reportFacade) {
        this.reportFacade = reportFacade;
    }


    @Override
    public ResponseEntity<?> apiV1ReportsBooksCarsFromFromDateTimeToToDateTimeGet(
            @PathVariable("fromDateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime fromDateTime,
            @PathVariable("toDateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime toDateTime) {
        try {
            val result = this.reportFacade.bookedCarsByFromDateTimeAndToDateTimeReport(fromDateTime, toDateTime);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> apiV1ReportsBooksCarsHoursFromFromDateToToDateGet(
            @PathVariable("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
            @PathVariable("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate) {
        try {
            val result = reportFacade.numberOfBookedCarsPerHourByFromDateToToDate(fromDate, toDate);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> apiV1ReportsBooksPaymentsTotalFromFromDateToToDateGet(
            @PathVariable("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
            @PathVariable("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate) {
        try {
            val result = this.reportFacade.totalPaymentByFromDateToToDate(fromDate, toDate);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }

}
