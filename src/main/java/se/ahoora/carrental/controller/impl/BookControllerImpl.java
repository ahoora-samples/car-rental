package se.ahoora.carrental.controller.impl;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import se.ahoora.carrental.controller.api.BookController;
import se.ahoora.carrental.dto.BookCreateDTO;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.facade.api.BookFacade;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.net.URI;

import static se.ahoora.carrental.Const.badRequest;
import static se.ahoora.carrental.Const.internalServerError;

@Slf4j
@CrossOrigin(origins = "*", maxAge = -1)
@Controller
public class BookControllerImpl implements BookController {
    private BookFacade bookFacade;

    @Autowired
    public BookControllerImpl(BookFacade bookFacade) {
        this.bookFacade = bookFacade;
    }


    @Override
    public ResponseEntity<?> apiV1BooksPost(@ApiParam(value = "", required = true) @Valid @RequestBody BookCreateDTO body, @ApiIgnore Errors errors) {
        try {
            if (errors.hasErrors())
                return badRequest(errors);
            val id = this.bookFacade.create(body);
            val headers = new HttpHeaders();
            headers.setLocation(new URI(String.format("/api/v1/books/%s", id)));
            return new ResponseEntity<Void>(null, headers, HttpStatus.CREATED);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }
}
