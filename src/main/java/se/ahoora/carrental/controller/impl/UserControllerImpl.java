package se.ahoora.carrental.controller.impl;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import se.ahoora.carrental.controller.api.UserController;
import se.ahoora.carrental.dto.UserCreateDTO;
import se.ahoora.carrental.dto.UserDTO;
import se.ahoora.carrental.dto.UserUpdateDTO;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.facade.api.UserFacade;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.net.URI;
import java.util.List;
import java.util.UUID;

import static se.ahoora.carrental.Const.badRequest;
import static se.ahoora.carrental.Const.internalServerError;

@Slf4j
@CrossOrigin(origins = "*", maxAge = -1)
@Controller
public class UserControllerImpl implements UserController {

    private UserFacade userFacade;

    @Autowired
    public UserControllerImpl(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Override
    public ResponseEntity<?> apiV1UsersPost(@ApiParam(value = "", required = true) @Valid @RequestBody UserCreateDTO body, @ApiIgnore Errors errors) {
        log.info("new user request");
        try {
            if (errors.hasErrors())
                return badRequest(errors);
            val id = this.userFacade.create(body);
            val headers = new HttpHeaders();
            headers.setLocation(new URI(String.format("/api/v1/users/%s", id)));
            return new ResponseEntity<Void>(null, headers, HttpStatus.CREATED);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> apiV1UsersPut(@Valid @RequestBody UserUpdateDTO body, @ApiIgnore Errors errors) {
        try {
            if (errors.hasErrors())
                return badRequest(errors);
            this.userFacade.update(body);
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> apiV1UsersGetId(@Valid @NotBlank String id) {
        try {
            UUID.fromString(id);
            val result = this.userFacade.findById(id);
            return new ResponseEntity<UserDTO>(result, HttpStatus.OK);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> apiV1UsersGet() {
        try {
            val result = this.userFacade.findAll();
            return new ResponseEntity<List<UserDTO>>(result, HttpStatus.OK);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> apiV1UsersDeleteId(@Valid String id) {
        try {
            this.userFacade.delete(id);
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }
}
