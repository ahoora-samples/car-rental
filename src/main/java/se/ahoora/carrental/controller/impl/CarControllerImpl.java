package se.ahoora.carrental.controller.impl;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import se.ahoora.carrental.controller.api.CarController;
import se.ahoora.carrental.dto.AvailabilityCreateDTO;
import se.ahoora.carrental.dto.CarCreateDTO;
import se.ahoora.carrental.dto.CarDTO;
import se.ahoora.carrental.exception.DataException;
import se.ahoora.carrental.facade.api.CarFacade;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;

import static se.ahoora.carrental.Const.badRequest;
import static se.ahoora.carrental.Const.internalServerError;

@Slf4j
@CrossOrigin(origins = "*", maxAge = -1)
@Controller
public class CarControllerImpl implements CarController {

    private CarFacade carFacade;

    @Autowired
    public CarControllerImpl(CarFacade carFacade) {
        this.carFacade = carFacade;
    }


    @Override
    public ResponseEntity<?> apiV1CarsPost(
            @ApiParam(value = "", required = true) @Valid @RequestBody CarCreateDTO body,
            @ApiIgnore Errors errors) {
        try {
            if (errors.hasErrors())
                return badRequest(errors);
            val id = this.carFacade.create(body);
            val headers = new HttpHeaders();
            headers.setLocation(new URI(String.format("/api/v1/cars/%s", id)));
            return new ResponseEntity<Void>(null, headers, HttpStatus.CREATED);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> apiV1CarsIdAvailabilitiesPost(
            @PathVariable("carId") String carId,
            @Valid AvailabilityCreateDTO body,
            Errors errors) {
        try {
            if (errors.hasErrors())
                return badRequest(errors);
            val aid = this.carFacade.addAvailability(carId, body);
            val headers = new HttpHeaders();
            headers.setLocation(new URI(String.format("/api/v1/cars/%s/availabilities/%s", carId, aid)));
            return new ResponseEntity<Void>(null, headers, HttpStatus.CREATED);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }


    @Override
    public ResponseEntity<?> apiV1CarsGet(
            @RequestParam(value = "from", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
            @RequestParam(value = "to", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
            @Valid @RequestParam(value = "maximumHourlyRate", required = false) @NumberFormat(pattern = "#.##") BigDecimal maximumHourlyRate) {
        try {
            val result = this.carFacade.search(from, to, maximumHourlyRate);
            return new ResponseEntity<List<CarDTO>>(result, HttpStatus.OK);
        } catch (DataException e) {
            log.warn(e.getMessage());
            return badRequest(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return internalServerError(e.getMessage());
        }
    }
}
