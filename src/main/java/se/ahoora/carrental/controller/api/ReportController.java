package se.ahoora.carrental.controller.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import se.ahoora.carrental.dto.BookedCarReportDTO;
import se.ahoora.carrental.dto.ErrorDTO;
import se.ahoora.carrental.dto.TotalDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface ReportController {


    @ApiOperation(value = "report of booked cars from date/time, to date/time", nickname = "apiV1ReportsFromFromDateTimeToToDateTimeGet", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = BookedCarReportDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/reports/books/cars/from/{fromDateTime}/to/{toDateTime}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    ResponseEntity<?> apiV1ReportsBooksCarsFromFromDateTimeToToDateTimeGet(
            @PathVariable("fromDateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime fromDateTime,
            @PathVariable("toDateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime toDateTime);


    @ApiOperation(value = "report of number of booked cars per hour from date, to date", nickname = "apiV1ReportsBooksPaymentsTotalFromFromDateToToDateGet", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = Object.class),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/reports/books/cars/hours/from/{fromDate}/to/{toDate}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    ResponseEntity<?> apiV1ReportsBooksCarsHoursFromFromDateToToDateGet(
            @PathVariable("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
            @PathVariable("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate);


    @ApiOperation(value = "report of total payment from date, to date", nickname = "apiV1ReportsBooksPaymentsTotalFromFromDateToToDateGet", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = TotalDTO.class),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/reports/books/payments/total/from/{fromDate}/to/{toDate}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    ResponseEntity<?> apiV1ReportsBooksPaymentsTotalFromFromDateToToDateGet(
            @PathVariable("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
            @PathVariable("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate);


}
