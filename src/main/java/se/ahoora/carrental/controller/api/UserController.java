package se.ahoora.carrental.controller.api;

import io.swagger.annotations.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import se.ahoora.carrental.dto.ErrorDTO;
import se.ahoora.carrental.dto.UserCreateDTO;
import se.ahoora.carrental.dto.UserDTO;
import se.ahoora.carrental.dto.UserUpdateDTO;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;


@Api(value = "user-api")
public interface UserController {

    @ApiOperation(value = "user registration", nickname = "apiV1UsersPost", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Status 201", response = Void.class),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/users", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    ResponseEntity<?> apiV1UsersPost(@ApiParam(value = "", required = true) @Valid @RequestBody UserCreateDTO body, @ApiIgnore Errors errors);


    @ApiOperation(value = "user update", nickname = "apiV1UsersPut", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Status 204", response = Void.class),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/users", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
    ResponseEntity<?> apiV1UsersPut(@ApiParam(value = "", required = true) @Valid @RequestBody UserUpdateDTO body, @ApiIgnore Errors errors);


    @ApiOperation(value = "user find by id", nickname = "apiV1UsersGetId", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = UserDTO.class),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    ResponseEntity<?> apiV1UsersGetId(@ApiParam(value = "", required = true) @Valid @PathVariable("id") String id);


    @ApiOperation(value = "user find all", nickname = "apiV1UsersGet", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = UserDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/users", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    ResponseEntity<?> apiV1UsersGet();


    @ApiOperation(value = "user delete", nickname = "apiV1UsersDeleteId", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Status 204", response = Void.class),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})

    @RequestMapping(value = "/api/v1/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.DELETE)
    ResponseEntity<?> apiV1UsersDeleteId(@ApiParam(value = "", required = true) @Valid @PathVariable("id") String id);


}
