package se.ahoora.carrental.controller.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import se.ahoora.carrental.dto.BookCreateDTO;
import se.ahoora.carrental.dto.ErrorDTO;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

public interface BookController {

    @ApiOperation(value = "book a car", nickname = "apiV1BooksPost", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Status 201", response = Void.class),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/books", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    ResponseEntity<?> apiV1BooksPost(
            @ApiParam(required = true) @Valid @RequestBody BookCreateDTO body,
            @ApiIgnore Errors errors);


}
