package se.ahoora.carrental.controller.api;

import io.swagger.annotations.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import se.ahoora.carrental.dto.AvailabilityCreateDTO;
import se.ahoora.carrental.dto.CarCreateDTO;
import se.ahoora.carrental.dto.CarDTO;
import se.ahoora.carrental.dto.ErrorDTO;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;


@Api(value = "car-api")
public interface CarController {

    @ApiOperation(value = "car registration", nickname = "apiV1CarsPost", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Status 201", response = Void.class),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/cars", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    ResponseEntity<?> apiV1CarsPost(
            @ApiParam(value = "", required = true) @Valid @RequestBody CarCreateDTO body,
            @ApiIgnore Errors errors);


    @ApiOperation(value = "car add new availability", nickname = "apiV1CarsIdAvailabilitiesPost", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Status 201", response = Void.class),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/cars/{carId}/availabilities", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    ResponseEntity<?> apiV1CarsIdAvailabilitiesPost(
            @PathVariable("carId") String carId,
            @ApiParam(value = "", required = true) @Valid @RequestBody AvailabilityCreateDTO body,
            @ApiIgnore Errors errors);


    @ApiOperation(value = "car find by date range and maximum hourly rate", nickname = "apiV1CarsGet", notes = "", response = Object.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = CarDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Status 400", response = ErrorDTO.class),
            @ApiResponse(code = 401, message = "Status 401", response = ErrorDTO.class),
            @ApiResponse(code = 403, message = "Status 403", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Status 500", response = ErrorDTO.class)})
    @RequestMapping(value = "/api/v1/cars", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    ResponseEntity<?> apiV1CarsGet(
            @RequestParam(value = "from", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
            @RequestParam(value = "to", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
            @Valid @RequestParam(value = "maximumHourlyRate", required = false) @NumberFormat(pattern = "#.##") BigDecimal maximumHourlyRate);

}
