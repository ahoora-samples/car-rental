package se.ahoora.carrental.service.impl;

import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import se.ahoora.carrental.domain.Availability;
import se.ahoora.carrental.domain.Car;
import se.ahoora.carrental.domain.User;
import se.ahoora.carrental.domain.UserType;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class BookServiceImplTest {

    @Autowired
    private BookServiceImpl service;

    @BeforeEach
    void beforeEach() {
        service = new BookServiceImpl(null, null, null);
    }

    @Test
    void car_deep_copy_test() {
        val owner = User.builder().address("address1").firstName("john").lastName("doe").username("john1").id(UUID.randomUUID().toString()).phoneNumber("0713").type(UserType.OWNER).build();

        val carId = UUID.randomUUID().toString();

        val avId = UUID.randomUUID().toString();

        final Map<String, Availability> avMap = new ConcurrentHashMap<>();

        final Map<String, String> attributes = new ConcurrentHashMap<>();
        attributes.put("color", "red");

        val av_rec_1 = Availability.builder()
                .id(avId)
                .carId(carId)
                .from(Instant.now().minus(10, ChronoUnit.DAYS))
                .to(Instant.now().plus(10, ChronoUnit.DAYS))
                .hourlyRate(new BigDecimal("130.00"))
                .build();

        avMap.put(avId, av_rec_1);

        var car = Car.builder()
                .id(carId)
                .owner(owner)
                .availabilities(avMap)
                .attributes(attributes)
                .plateNumber("GUN960")
                .build();

        val newCar = service.copy(car);
        assertEquals(newCar, car);
    }
}
