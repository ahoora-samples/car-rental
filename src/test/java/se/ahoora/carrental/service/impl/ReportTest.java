package se.ahoora.carrental.service.impl;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class ReportTest {


    @Test
    public void grouping_count_test() {
        val start = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC);
        val end = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC).plusHours(5);
        val start1 = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC);
        val end1 = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC).plusHours(6);

        Map<LocalDateTime, Long> collect = Stream
                .of(extractDatesFromRangePerHour(start, end), extractDatesFromRangePerHour(start1, end1))
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()));

        System.out.println(collect.toString());
    }

    List<LocalDateTime> extractDatesFromRangePerHour(LocalDateTime from, LocalDateTime to) {
        return Stream.iterate(from, date -> date.plusHours(1))
                .limit(ChronoUnit.HOURS.between(from, to))
                .map(i -> i.withMinute(0).withSecond(0).withNano(0))
                .collect(Collectors.toList());
    }

}
