package se.ahoora.carrental.repository.impl;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.ahoora.carrental.domain.Availability;
import se.ahoora.carrental.domain.Car;
import se.ahoora.carrental.domain.User;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;


@Slf4j
class CarRepoImplTest {

    private CarRepoImpl repo;
    private List<Availability> input;


    @BeforeEach
    void beforeEach() {
        repo = new CarRepoImpl();
        input = Arrays.asList(
                Availability.builder()
                        .hourlyRate(new BigDecimal("100.00"))
                        .from(Instant.now().minus(1, ChronoUnit.DAYS))
                        .to(Instant.now().plus(1, ChronoUnit.DAYS)).build(),

                Availability.builder()
                        .hourlyRate(new BigDecimal("110.00"))
                        .from(Instant.now().minus(2, ChronoUnit.DAYS))
                        .to(Instant.now().plus(1, ChronoUnit.DAYS)).build(),

                Availability.builder()
                        .hourlyRate(new BigDecimal("120.00"))
                        .from(Instant.now().minus(3, ChronoUnit.DAYS))
                        .to(Instant.now().plus(1, ChronoUnit.DAYS)).build());


    }


    @Test
    void searchFromCriteria_from_1_test() {
        assertEquals(3, repo.searchDateCriteria(input, null, null).size());
    }

    @Test
    void searchFromCriteria_from_2_test() {
        assertEquals(3, repo.searchDateCriteria(input, Instant.now().minus(3, ChronoUnit.DAYS), null).size());
    }

    @Test
    void searchFromCriteria_from_3_test() {
        assertEquals(2, repo.searchDateCriteria(input, Instant.now().minus(2, ChronoUnit.DAYS), Instant.now().plus(1, ChronoUnit.DAYS)).size());
    }

    @Test
    void searchMaxRateCriteria_50_test() {
        val maxRate = new BigDecimal("50.00");
        assertEquals(0, repo.searchMaxRateCriteria(input, maxRate).size());
    }

    @Test
    void searchMaxRateCriteria_100_test() {
        val maxRate = new BigDecimal("100.00");
        assertEquals(1, repo.searchMaxRateCriteria(input, maxRate).size());
    }

    @Test
    void searchMaxRateCriteria_110_test() {
        val maxRate = new BigDecimal("110.00");
        assertEquals(2, repo.searchMaxRateCriteria(input, maxRate).size());
    }

    @Test
    void searchMaxRateCriteria_120_test() {
        val maxRate = new BigDecimal("120.00");
        assertEquals(3, repo.searchMaxRateCriteria(input, maxRate).size());
    }

    @Test
    void searchMaxRateCriteria_300_test() {
        val maxRate = new BigDecimal("300.00");
        assertEquals(3, repo.searchMaxRateCriteria(input, maxRate).size());
    }

    @Test
    void searchMaxRateCriteria_null_test() {
        assertEquals(3, repo.searchMaxRateCriteria(input, null).size());
    }

    @Test
    void search_test() {
        Map<String, Availability> av1 = new ConcurrentHashMap<>();
        Map<String, Availability> av2 = new ConcurrentHashMap<>();
        val carId_1 = UUID.randomUUID().toString();
        val carId_2 = UUID.randomUUID().toString();


        val avId_1 = UUID.randomUUID().toString();
        av1.put(avId_1, Availability.builder()
                .id(avId_1)
                .carId(carId_1)
                .from(Instant.now().minus(20, ChronoUnit.DAYS))
                .to(Instant.now().plus(20, ChronoUnit.DAYS))
                .hourlyRate(new BigDecimal("50.00"))
                .build());

        val avId_2 = UUID.randomUUID().toString();
        av1.put(avId_2, Availability.builder()
                .id(avId_2)
                .carId(carId_2)
                .from(Instant.now().minus(10, ChronoUnit.DAYS))
                .to(Instant.now().plus(10, ChronoUnit.DAYS))
                .hourlyRate(new BigDecimal("130.00"))
                .build());


        val avId_3 = UUID.randomUUID().toString();
        av2.put(avId_3, Availability.builder()
                .id(avId_3)
                .carId(carId_1)
                .from(Instant.now().minus(2, ChronoUnit.DAYS))
                .to(Instant.now().plus(2, ChronoUnit.DAYS))
                .hourlyRate(new BigDecimal("200.00"))
                .build());

        val avId_4 = UUID.randomUUID().toString();
        av2.put(avId_4, Availability.builder()
                .id(avId_4)
                .carId(carId_2)
                .from(Instant.now().minus(1, ChronoUnit.DAYS))
                .to(Instant.now().plus(1, ChronoUnit.DAYS))
                .hourlyRate(new BigDecimal("150.00"))
                .build());

        var c1 = Car.builder().id(carId_1).owner(User.builder().build()).availabilities(av1).plateNumber("GUN960").id(carId_1).build();
        var c2 = Car.builder().id(carId_2).owner(User.builder().build()).availabilities(av2).plateNumber("TOM555").id(carId_1).build();

        Map<String, Car> table = new ConcurrentHashMap<>();
        table.put(carId_1, c1);
        table.put(carId_2, c2);
        repo.setTable(table);

        val from = Instant.now().minus(1, ChronoUnit.DAYS);
        val to = Instant.now().plus(1, ChronoUnit.DAYS);
        val maxRate = new BigDecimal("130.00");


        val flatData = repo.flattenAvailabilities(table);
        assertEquals(4, flatData.size());
        assertEquals(2, repo.searchMaxRateCriteria(flatData, maxRate).size());
        assertEquals(4, repo.searchMaxRateCriteria(flatData, null).size());
        assertEquals(1, repo.searchDateCriteria(flatData, from, to).size());
        assertEquals(4, repo.searchDateCriteria(flatData, null, to).size());
        assertEquals(1, repo.searchDateCriteria(flatData, from, null).size());
        assertEquals(4, repo.searchDateCriteria(flatData, null, null).size());
        val a = repo.searchMaxRateCriteria(flatData, maxRate);
        val b = repo.searchDateCriteria(a, null, null);
        assertEquals(2, b.size());

        assertEquals(2, repo.getCarWithSingleAvailability(b).size());
    }

}
