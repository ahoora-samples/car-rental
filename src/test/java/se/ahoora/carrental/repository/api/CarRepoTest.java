package se.ahoora.carrental.repository.api;

import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import se.ahoora.carrental.domain.Availability;
import se.ahoora.carrental.domain.Car;
import se.ahoora.carrental.domain.User;
import se.ahoora.carrental.domain.UserType;
import se.ahoora.carrental.exception.DataException;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CarRepoTest {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private CarRepo carRepo;

    @BeforeEach
    void beforeEach() {
        this.userRepo.setTable(new ConcurrentHashMap<>());
        this.carRepo.setTable(new ConcurrentHashMap<>());
    }

    @Test
    @DisplayName("userRepo crud test")
    void car_new_test() {
        assertEquals(0, userRepo.findAll().size());
        val user = User.builder()
                .firstName("john")
                .lastName("doe")
                .address("stockholm")
                .phoneNumber("07312345")
                .username("john")
                .type(UserType.OWNER)
                .build();

        this.userRepo.create(user);

        Map<String, String> attributes = new ConcurrentHashMap<>();
        attributes.put("brand", "bmw");
        attributes.put("color", "red");
        attributes.put("model", "z3");
        attributes.put("year", "2019");

        Map<String, Availability> availabilities = new ConcurrentHashMap<>();
        val aId = UUID.randomUUID().toString();
        availabilities.put(aId,
                Availability.builder().id(aId).from(Instant.now()).to(Instant.now())
                        .hourlyRate(new BigDecimal("200.00")).build());

        val PLATE_NUMBER = "GUN960";

        val car0 = Car.builder()
                .plateNumber(PLATE_NUMBER)
                .attributes(attributes)
                .availabilities(availabilities)
                .owner(user)
                .build();

        this.carRepo.create(car0);

        val car1 = Car.builder()
                .plateNumber(PLATE_NUMBER)
                .attributes(attributes)
                .availabilities(availabilities)
                .owner(user)
                .build();

        val duplicatedPlateNumberException = assertThrows(DataException.class, () -> this.carRepo.create(car1));
        assertTrue(duplicatedPlateNumberException.getMessage().startsWith("the plate number has been registered"));

    }


}
