package se.ahoora.carrental.repository.api;

import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import se.ahoora.carrental.domain.User;
import se.ahoora.carrental.domain.UserType;
import se.ahoora.carrental.exception.DataException;

import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class UserRepoTest {

    @Autowired
    private UserRepo userRepo;


    @BeforeEach
    void beforeEach() {
        this.userRepo.setTable(new ConcurrentHashMap<>());
    }

    @Test
    @DisplayName("userRepo crud test")
    void user_crud_test() {
        assertEquals(0, userRepo.findAll().size());
        val actualUser = User.builder()
                .firstName("john")
                .lastName("doe")
                .address("stockholm")
                .phoneNumber("07312345")
                .username("john")
                .type(UserType.OPERATOR)
                .build();

        val userId = this.userRepo.create(actualUser);
        assertEquals(1, userRepo.findAll().size());
        val expectedUser = userRepo.findById(userId);
        assertEquals(expectedUser, actualUser);
        userRepo.delete(userId);
        assertEquals(0, userRepo.findAll().size());
        Exception exception = assertThrows(DataException.class, () -> {
            userRepo.findById(userId);
        });
        assertTrue(exception.getMessage().startsWith("user not found"));
    }

}