package se.ahoora.carrental.controller.api;

import io.restassured.http.ContentType;
import org.springframework.boot.test.context.SpringBootTest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class UserControllerTest {


//
//    @Test
    public void users_find_all_test() {
        final String expectedResponseBody = "";
        given().log().all()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/api/v1/users").then().log().all()
//                .assertThat()
                .body(containsString(expectedResponseBody));
    }

}
