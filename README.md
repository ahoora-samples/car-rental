### Build and run the car-rental project

__Notes:__ 

1- Please clone the source, do not download the compressed version. docker needs the `.git` folder.  

2- The design is based on the OOP concepts, not the Relational Database.

3- I used `openjdk version 11.0.8` for develop and build the application.

---
- run only the basic unit tests, and will stop the build if any of them fails

```bash
./mvnw clean test
``` 
---
- run integration tests, and will not stop the build if any of them fails
```bash
./mvnw clean integration-test
```
---
- run integration tests, and will stop the build if an integration test fails
```bash
./mvnw clean verify
```
---
- run the application with maven
```bash
./mvnw spring-boot:run
```
 
---
- build the docker image 

  it will create an image with 2 different tags, the `latest` tag and the [git commit id abbrev] tag e.g. `66b4154`

```bash
./mvnw clean deploy
```
---
- run the docker container
```bash
docker run -d --name car-rental -p 8080:8080 -v $HOME/car-rental-ahoora-log:/log infor/car-rental:latest
```
---
- other docker commands
```bash
docker logs -f car-rental

docker stop car-rental

docker rm car-rental
```
---
- health check
```bash
curl http://localhost:8080/actuator/health
```
---
- generate the `jmeter` files using the `swagger` json document.

find the `openapi-generator-cli` installation document [here](https://openapi-generator.tech/docs/installation/).


```bash
openapi-generator-cli generate -i http://localhost:8080/api-docs -g jmeter
```

you can find the generated files in `carRental/etc/jmeter` folder

---
### Use the project

you can use it either with `browser` or `Postman`

- Browser
  Open the browser and paste [http://localhost:8080/](http://localhost:8080/) in browser address bar. 

  it will redirect to `Swagger UI` page. 

  click on e.g. `user-controller-impl > /api/v1/users` links. 

  now you can `try it out`.

- Postman
  open `Postman` and click on `Import > Import from link`.
  
  paste the [http://localhost:8080/api-docs](http://localhost:8080/api-docs) in Url text box and click `Import`.
  
  It will import the `Swagger` data into the Postman.
  
  find the `harry kart play calculation` resource and click on it.
  
  now you can `try it out`.

---

- ASCII art: [car-rental](http://patorjk.com/software/taag/#p=display&h=1&v=1&f=Slant&t=infor%20car%20rental)